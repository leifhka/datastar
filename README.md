
<div align="center">

![image](datastar_scaled_10x.png)

</div>

An educational multiplayer text-based space sim written and
played in SQL! Imagine playing Elite or other similar
space/economy sims using only SQL and you have Datastar.

The entire game is implemented and played within a
PostgreSQL database that is constantly changing,
and that simulates an economy with trading, mining,
ship upgrades, NPCs and other players. Players must explore
the universe, gather data in their own database schema,
and make decisions based on this data, with all PostgreSQL
features available. Everything the player can do can also
be scripted, so the main goal of the game is to make the
best script that generates the most money.

The game supports multiple players playing in the same database,
has a chat, and a highscore table.

## Playing the game

The game is played by logging into a database running
the Datastar-game, and executing commands and queries.
Thus, the game is playing using any query interface
for PostgreSQL-databases, such as `psql` or [Edbit](https://gitlab.com/leifhka/edbit/).
Edbit has some features which are very helpful when playing,
such as monitored windows (more on this below).

To create a player you first have to log in to the
database running Datastar as the user with name
`player_creator` with password `player_creator`. Then
execute:

```sql
SELECT create_player(
    'hackzor', -- desired username for player
    'pwd123',  -- password for the user for logging into the DB
    1          -- Faction (ID) the player wants to play as
);
```

More information on the different factions can be seen in
the `factions`-table.

Note that the factions have slightly different starting statistics,
according to the description (e.g. `sznerckk` is more efficient miners
than the other factions).

Then, when a user is created, log out and login using your
newly created user and password.

The player can use the information in the tables described
in

```sql
SELECT * FROM game_tables;
```

and act using the functions described in

```sql
SELECT * FROM game_functions;
```

The game is turn-based/tick-based, and each turn/tick the
player can move once (via `fly` or `warp`) and do one action
(`mine`, `buy`, `sell`) in any order. Ticks are executed on
a regular interval (typically every 5 seconds).

To execute a function, simply wrap it in a `SELECT`-clause.
E.g.~to first fly to planet `3`, then `mine`, then advance
to next tick, simply execute the commands:

```sql
SELECT fly(3); SELECT mine();
```

These can, of course, also be put into the same query:

```sql
SELECT fly(3), mine();
```

Note that it is also possible to have the arguments to these
functions be retrieved via a query, e.g.:

```sql
SELECT fly(id), mine()
FROM planet
WHERE mine_stock = 'rich'
LIMIT 1;
```

The goal of the game is to maximize the `money_rating` (as
can be seen in the `status`-table), which is a measure of
how much money you have compared to all other actors in the
universe. The universe contains many other NPCs and players that are
also acting every turn, these needs to be taken into account
when planning a strategy. To gain money, one can e.g.~buy a
resource cheap and sell for a higher price to a different
planet (needing the resource more), or mine resources and
sell to a needing planet.

It is also possible to chat with other players via the
`chat`-table and `chat`-function (see `game_tables` and
`game_functions` described above for more details),
so cooperation is also possible.

Helpful game features and help on getting started is
described below.

## Tick-commands

At any point in time, the player can only get information
local to the player. E.g.~the player can only view the
market at the player's current planet. You can of course
store historical information in your own tables, e.g.:

```sql
CREATE TABLE historical_prices AS
SELECT * FROM market;
```

and then, on each tick execute

```sql
INSERT INTO historical_prices
SELECT * FROM market;
```

However, there might be a lot of historical information one
wants to store. Therefore, the game has a feature called
tick-commands. A tick-command is a normal player-defined
SQL-command that is executed at every tick, that can be any
`INSERT`, `UPDATE`, `DELETE`, etc. command. Tick-commands
are created and deleted with the functions
`insert_tick_command` and `delete_tick_command`. So to store
all markets seen, execute:

```sql
CREATE TABLE historical_markets(
    tick int,
    planet text,
    system text, -- context
    id int,
    resource text,
    sell_price float,
    buy_price float -- market
);
SELECT insert_tick_command(
    'markets',             -- name of tick command (used for deletion)
    1,                     -- execution order of your tick commands
    'INSERT INTO historical_markets
     SELECT t.tick, s.cur_planet, s.cur_system,
            r.id, r.resource, r.sell_price, r.buy_price
     FROM status AS s, market AS r, current_tick AS t;'
);
```
Note that we add the current tick to each line of the
table, so that we know when the information was recorded.
This also makes it possible to find the newest price
recorded, etc.

Now, all markets for planets you have visited will be stored
in the `historical_markets` table. For more suggestions on
convenient tick commands, see the next section.

Note that time consuming queries cost (in-game) money,
so players should try to optimize their queries as
much as possible. Commands start to cost money
if they take longer than 0.1 seconds, and grows
exponentially after that, where the actual cost
depends on how much money you currently have
(however, the maximum cost is never greater than
10% of your total money). Details on cost of
your tick-commands and errors can be seen in the 
`system_message` relation.

## Starting out

This section outlines a suggested way to start playing for
the first time. Start by executing the following commands,
that will set up some useful tick-commands and `VIEW`s:

```sql
-- Store all seen markets using tick-command:
CREATE TABLE <username>.historical_markets(
    tick int,
    pid int,
    planet text,
    system text, 
    rid int,
    resource text,
    sell_price float,
    buy_price float -- market info
);
SELECT insert_tick_command(
    'markets',
    1,
    'INSERT INTO <username>.historical_markets
     SELECT t.tick, p.id, s.cur_planet,
            s.cur_system, r.id, r.resource,
            r.sell_price, r.buy_price
     FROM status AS s JOIN planets AS p ON (s.cur_planet = p.name),
          market AS r,
          current_tick AS t;'
);

-- Set up a VIEW that shows best buy-sell route in current solar system:
CREATE VIEW <username>.best_route AS
SELECT b.tick AS buy_tick,
       b.pid AS buy_from,
       s.tick AS sell_tick,
       s.pid AS sell_to,
       rid,
       resource,
       s.sell_price - b.buy_price AS profit
FROM <username>.historical_markets AS b
     JOIN <username>.historical_markets AS s USING (rid, resource)
WHERE b.pid != s.pid
ORDER BY profit DESC;

-- Make a tick-command to fly to all planets in your
-- current solar system where we do not have any historical
-- market, one for each tick:
SELECT insert_tick_command(
    'round-trip',
    2, -- happens after the tick-command above
    'SELECT fly(p.id) -- fly to planet nr. id
     FROM planets AS p, status
     WHERE NOT moved AND -- only if not moved this tick
           p.id NOT IN (SELECT pid FROM <username>.historical_markets)
     LIMIT 1;'
);
```

where `<username>` is your username (all players have a schema
with the username as schema for which the player has all
privileges).

(Note that the above commands can be put into a script for
easy execution in other games if you start over.)

You can now make trade rounds with the following
tick-commands:

```sql
SELECT insert_tick_command(
    'trade-round-buy',
    3,
    'SELECT fly(buy_from),
            buy(rid, load_max),
            set_state(sell_to::text) -- set where to sell next tick
     FROM <username>.best_route, status
     WHERE NOT moved AND NOT acted AND
           load_type IS NULL
     ORDER BY profit DESC
     LIMIT 1;'
);
SELECT insert_tick_command(
    'trade-round-sell',
    4,
    'SELECT fly(state::int), -- fly to planet set above
            sell_all()
     FROM status
     WHERE NOT moved AND NOT acted AND
           load_type IS NOT NULL;'
);
```
Note that the `'trade-round-buy'`-command is
only executed if you don't have already bought something,
and the `'trade-round-sell'` is only executed if you
have bought something. Thus, only of these is executed
every tick, and each is executed the tick following
the execution of the other.

If you are using Edbit, now is also a good time to create
several monitored windows keeping track of you status,
earnings, and how profitable your trade-routes actually are.
(See below for more info on this.)

Note that as time goes by (tick by tick) your information
will be outdated, i.e. the prices stored in
`<username>.historical_markets` are no longer representative, as you
and other actors have bought and sold resources and the
planets have consumed resources, all affecting the prices.
Also, the `<username>.historical_markets`-table will
become quite large, making queries over it expensive
(in both time and money).
You can make similar round-trips as above (either by
executing a `TRUNCATE <username>.hitsorical_markets;` or
by updating the `round-trip`-command to also take into account how old the
information is, based on the `tick`-column) to collect new
information from all planets. Deleting the
`<username>.historical_markets`-table can also be done
regularly with a tick-command e.g. as follows:

```sql
SELECT insert_tick_command(
  'delete_markets',
  5,
  'DELETE FROM <username>.historical_markets
   WHERE (SELECT tick FROM current_tick) % 50 = 0;' -- executes every 50 tick
);                                                  -- and deletes all rows
```

Once you have made a decent amount of money, you can think
about upgrading your ship, try out mining, or warping to
other systems. Note that if you warp to a different system,
the `<username>.best_route`-view no longer works, as it does not
contain information about which planet is in which system,
and does not limit the results to the system you are in. Try
to fix these issues, and in general automate as much as
possible, create tables and tick-commands to gather all
relevant information, and make a query that always performs
the best possible action given the information you have.

You can always see you standing in the `money_rating`-column
in the `status`-view or in the `highscore`-table.

Good luck, good learning, and have fun! :)

## Using Edbit

When playing, one typically needs a lot of information to
make good decisions. It can be tedious to write the same
queries over and over, even though one typically only need
to press Ctrl-P to browse previously written queries. Edbit
supports monitored queries that are windows containing a query
and its answer, and which is re-executed every time a
query/command is executed in the main window. One can then
make many such windows, and arrange them across your
screen. This lets you have several continuously updated
(i.e.~on every command you execute) queries, always visible.
To create such a monitored window, simply write a query (in
the main Edbit window), execute it, and press the `M`-key.
Typically, one would want at least the following two queries
open in separate monitored windows:

```sql
SELECT * FROM status;
SELECT * FROM current_system;
```

Below is a screenshot of how this would look with the two
queries above together with a query finding the 10 closest
systems as monitored windows to the right of the screen,
with the main window on the left:

![Monitored windows in Edbit](./monitored_windows.png)

Note that the queries are only re-executed whenever a
command/query is executed in the main window, thus
to refresh all windows, simply execute

```sql
SELECT 1;
```

or something similar. (Note: Monitored queries that
re-execute based on timed intervals is a
[planned feature of Edbit](https://gitlab.com/leifhka/edbit/-/issues/2),
but as of writing not yet implemented.)

For more details on how to use Edbit, see the README-file in
[Edbit's repo](https://gitlab.com/leifhka/edbit).

## Local installation

(Note: This section describes how to setup a new
instance/server of the game. If you only want to play the game,
try to find an already existing instance.)

The game requires PostgreSQL (tested on version 17,
but may work on older versions as well)
with the [pg_cron extension](https://github.com/citusdata/pg_cron).

To setup an instance of the game, clone the repo

```
git clone https://gitlab.com/leifhka/datastar 
```

Then execute the `datastar.sql` in a database:

```
cd datastar/src/ psql <flags> -f datastar.sql
```

where `<flags>` are the connection flags (database name,
username, host, etc.). This will set up the game's universe
of factions, systems, planets, markets, etc. and a player-creator
user. Then, execute

```sql
-- Execute SELECT tick(); every 5 seconds
SELECT cron.schedule(
    'tick-update',
    '5 seconds',
    'SELECT tick();'
);
```

If you want to start a new game, unschedule
the tick-commands, execute the
`clean.sql`-script to delete all data, then re-execute
the `datastar.sql`-script, i.e. from `datastar/src`,
and finally reschedule the tick-commands:

```
psql <flags> -c "SELECT cron.unschedule('tick-update');"
psql <flags> -f clean.sql
psql <flags> -f datastar.sql
psql <flags> -c "SELECT cron.schedule('tick-update','5 seconds','SELECT tick();');"
```

### Updating to new Datastar version

Datastar is in constant development, so new updates come
every now and then. It is currently not possible to update
to a new version and keep your current game, thus the
following update procedure will clean the database and set
up a new game with the newest version.

To update the game, execute these commands from the
`datastar/src`-folder in order:

```
psql <flags> -c "SELECT cron.unschedule('tick-update');"
psql <flags> -f clean.sql     # Execute old clean-script
git pull origin main          # Update game-files
psql <flags> -f datastar.sql  # Create new game with updated game
psql <flags> -c "SELECT cron.schedule('tick-update','5 seconds','SELECT tick();');"
```
