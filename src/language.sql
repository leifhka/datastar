CREATE SCHEMA lang;

-- Universal

CREATE TABLE lang.letter(letter text, vowl boolean);
INSERT INTO lang.letter VALUES
('a', true), ('b', false), ('c', false), ('d', false),
('e', true), ('f', false), ('g', false), ('h', false),
('i', true), ('j', false), ('k', false), ('l', false),
('m', false), ('n', false), ('o', true), ('p', false),
('q', false), ('r', false), ('s', false), ('t', false),
('u', true), ('v', false), ('w', false), ('x', false),
('y', true), ('z', false);

CREATE VIEW lang.vowl(letter) AS
SELECT letter FROM lang.letter WHERE vowl;

CREATE VIEW lang.consonant(letter) AS
SELECT letter FROM lang.letter WHERE NOT vowl;

-- Sznerckk

CREATE TABLE lang.sznerckk_c(letter text);

INSERT INTO lang.sznerckk_c
SELECT letter FROM lang.consonant;

INSERT INTO lang.sznerckk_c VALUES
('br'), ('bl'),
('cr'), ('ch'), ('cl'), ('cr'),
('dr'), ('dv'), ('dw'), ('dh'),
('fj'), ('fl'), ('fn'), ('fr'), ('fh'),
('gl'), ('gn'), ('gr'), ('gv'), ('gw'), ('gh'),
('kl'), ('kn'), ('kr'), ('kv'), ('kw'),
('pl'), ('pr'), ('qr'), ('rckk'), ('rkk'),
('sc'), ('sj'), ('sl'), ('sn'), ('sv'), ('sw'), ('szn'), 
('str'), ('skj'), ('skr'), ('sch');

CREATE TABLE lang.sznerckk_v(letter text);
INSERT INTO lang.sznerckk_v
SELECT letter FROM lang.vowl;

CREATE VIEW lang.sznerckk_word(word) AS
SELECT c1.letter || v.letter || c2.letter
FROM lang.sznerckk_c AS c1,
     lang.sznerckk_v AS v,
     lang.sznerckk_c AS c2;
  

-- Ooulium

CREATE TABLE lang.ooulium_c(letter text);
INSERT INTO lang.ooulium_c VALUES
('b'), ('d'), ('f'), ('g'),
('j'), ('l'), ('m'), ('n'),
('s'), ('v'), ('w');

CREATE TABLE lang.ooulium_v(letter text);
INSERT INTO lang.ooulium_v
SELECT letter FROM lang.vowl
UNION ALL
SELECT v1.letter || v2.letter
FROM lang.vowl AS v1, lang.vowl AS v2
UNION ALL
SELECT v1.letter || v2.letter || v3.letter
FROM lang.vowl AS v1, lang.vowl AS v2, lang.vowl AS v3;

CREATE VIEW lang.ooulium_word(word) AS
SELECT v1.letter || c.letter || v2.letter
FROM lang.ooulium_v AS v1,
     lang.ooulium_c AS c,
     lang.ooulium_v AS v2
UNION ALL
SELECT v1.letter || c1.letter || v2.letter || c2.letter
FROM lang.ooulium_v AS v1,
     lang.ooulium_c AS c1,
     lang.ooulium_v AS v2,
     lang.ooulium_c AS c2;

-- Sonos

CREATE TABLE lang.sonos_c(letter text);
INSERT INTO lang.sonos_c
SELECT letter FROM lang.consonant;

CREATE TABLE lang.sonos_v(letter text);
INSERT INTO lang.sonos_v
SELECT letter FROM lang.vowl;

CREATE VIEW lang.sonos_word(word) AS
WITH
  vw3(letter) AS (
    SELECT c.letter || v.letter || c.letter
    FROM lang.sonos_c AS c,
         lang.sonos_v AS v
  ),
  co3(letter) AS (
    SELECT v.letter || c.letter || v.letter
    FROM lang.sonos_c AS c,
         lang.sonos_v AS v
  ),
  vw5(letter) AS (
    SELECT c.letter || v2.letter || c.letter
    FROM lang.sonos_c AS v1,
         lang.sonos_v AS c,
         vw3 AS v2
  ),
  co5(letter) AS (
    SELECT c1.letter || v.letter || c2.letter || v.letter || c1.letter
    FROM lang.sonos_c AS c1,
         lang.sonos_v AS v,
         co3 AS c2
  )
SELECT * FROM vw3
UNION 
SELECT * FROM vw5
UNION 
SELECT * FROM co5;

-- Carbonae

CREATE TABLE lang.carbonae_cb(letter text);

INSERT INTO lang.carbonae_cb
SELECT letter FROM lang.consonant WHERE letter != 'x';

INSERT INTO lang.carbonae_cb VALUES
('br'), ('bl'),
('cr'), ('ch'), ('cl'), ('cr'),
('dr'), 
('fj'), ('fl'), ('fr'),
('gl'), ('gr'), 
('kl'), ('kn'), ('kr'), ('kv'), 
('pl'), ('pr'), 
('sc'), ('sl'), ('sn'), ('sv'), ('sw'), ('st'),
('str'), ('skr');

CREATE TABLE lang.carbonae_ce(letter text);

INSERT INTO lang.carbonae_ce
SELECT letter FROM lang.consonant WHERE letter != 'h';

INSERT INTO lang.carbonae_ce VALUES
('rb'), ('lb'),
('ch'), ('ck'),
('rd'), 
('lf'), ('rf'),
('rg'), 
('lk'), ('nk'), ('rk'), 
('lp'), ('rp'), 
('ns'), ('st'), ('tch');

CREATE TABLE lang.carbonae_v(letter text);
INSERT INTO lang.carbonae_v
SELECT letter FROM lang.vowl;

CREATE VIEW lang.carbonae_word(word) AS
SELECT c1.letter || v.letter || c2.letter
FROM lang.carbonae_cb AS c1,
     lang.carbonae_v AS v,
     lang.carbonae_ce AS c2;
  

-- All words by faction

CREATE VIEW lang.words(word, faction) AS
SELECT word, 'sznerckk' FROM lang.sznerckk_word
UNION ALL
SELECT word, 'ooulium' FROM lang.ooulium_word
UNION ALL
SELECT word, 'sonos' FROM lang.sonos_word
UNION ALL
SELECT word, 'carbonae' FROM lang.carbonae_word;

CREATE FUNCTION lang.pick_words(fac text, num int) RETURNS TABLE(word text, faction text) AS
$body$
BEGIN
  RETURN QUERY EXECUTE format(
      'SELECT word, ''%s'' FROM lang.%s_word ORDER BY random() LIMIT %s;',
      fac, fac, num
  );
END;
$body$ language plpgsql;
