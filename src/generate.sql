-- Generation script for making a new universe

CREATE FUNCTION dss.pick_number(mn int, mx int) RETURNS int AS
$body$
  SELECT round(random()*(mx-mn)+mn)::int;
$body$ language sql;

CREATE TABLE dss.words(word text PRIMARY KEY, faction text);

INSERT INTO dss.words
SELECT word, 'sznerckk' FROM lang.sznerckk_word ORDER BY random() LIMIT 1000
ON CONFLICT (word) DO NOTHING;
INSERT INTO dss.words
SELECT word, 'ooulium' FROM lang.ooulium_word ORDER BY random() LIMIT 1000
ON CONFLICT (word) DO NOTHING;
INSERT INTO dss.words
SELECT word, 'carbonae' FROM lang.carbonae_word ORDER BY random() LIMIT 1000
ON CONFLICT (word) DO NOTHING;
INSERT INTO dss.words
SELECT word, 'sonos' FROM lang.sonos_word ORDER BY random() LIMIT 1000
ON CONFLICT (word) DO NOTHING;

CREATE FUNCTION dss.pick_name(fac text) RETURNS text AS
$body$
  DELETE FROM dss.words
  WHERE word = (
    SELECT word FROM dss.words WHERE faction = fac
    ORDER BY random() LIMIT 1
  )
  RETURNING (word);
$body$ language sql;

-- Resources 

\echo 'Generating resources...'

\set n_res 20

INSERT INTO dss.resource(name, faction, category, value)
SELECT dss.pick_name('sznerckk') AS name, 'sznerckk' AS faction,
  (SELECT name FROM dss.category OFFSET mod(n.v, (SELECT count(*) FROM dss.category)) LIMIT 1) AS category,
  (0.1 + random())*10 AS value
FROM
  generate_series(1, :n_res) AS n(v)
UNION ALL
SELECT dss.pick_name('ooulium') AS name, 'ooulium' AS faction, 
  (SELECT name FROM dss.category OFFSET mod(n.v, (SELECT count(*) FROM dss.category)) LIMIT 1) AS category,
  (0.1 + random())*10 AS value
FROM
  generate_series(1, :n_res) AS n(v)
UNION ALL
SELECT dss.pick_name('sonos') AS name, 'sonos' AS faction, 
  (SELECT name FROM dss.category OFFSET mod(n.v, (SELECT count(*) FROM dss.category)) LIMIT 1) AS category,
  (0.1 + random())*10 AS value
FROM
  generate_series(1, :n_res) AS n(v)
UNION ALL
SELECT dss.pick_name('carbonae') AS name, 'carbonae' AS faction,
  (SELECT name FROM dss.category OFFSET mod(n.v, (SELECT count(*) FROM dss.category)) LIMIT 1) AS category,
  (0.1 + random())*10 AS value
FROM
  generate_series(1, :n_res) AS n(v);

-- Systems

\echo 'Generating systems...'

CREATE FUNCTION dss.pick_point_within(sx float, ex float, sy float, ey float)
RETURNS float[] AS
$body$
  SELECT ARRAY[(ex-sx)*random() + sx, (ey-sy)*random() + sy];
$body$ language sql;

INSERT INTO dss.system(name, faction, pos)
SELECT dss.pick_name('sznerckk'), 'sznerckk', dss.pick_point_within(0, 600, 0, 600)
FROM generate_series(1, 10) t
UNION ALL
SELECT dss.pick_name('ooulium'), 'ooulium', dss.pick_point_within(0, 600, 400, 1000)
FROM generate_series(1, 10) t
UNION ALL
SELECT dss.pick_name('sonos'), 'sonos', dss.pick_point_within(400, 1000, 0, 600)
FROM generate_series(1, 10) t
UNION ALL
SELECT dss.pick_name('carbonae'), 'carbonae', dss.pick_point_within(400, 1000, 400, 1000)
FROM generate_series(1, 10) t;

-- Planets and stocks

\echo 'Generating planets...'

INSERT INTO dss.planet(id, name, system, symbol, mine_type, mine_stock, provides_upgrades,
                       pos, receptionist_name, receptionist_portrait)
SELECT
  p.n AS id,
  dss.pick_name(s.faction) AS name,
  s.name AS system,
  (SELECT symbol FROM dss.planet_symbol WHERE nr = p.n),
  mt.mine_type, 
  random()*1000000 AS mine_stock,
  (random()*10 >= 8) AS provides_upgrades,
  dss.pick_point_within(s.pos[1] - random(), s.pos[1] + random(),
                        s.pos[2] - random(), s.pos[2] + random()) AS pos,
  dss.pick_name(s.faction) AS receptionist_name,
  (SELECT portrait FROM dss.portrait AS po WHERE po.faction = s.faction ORDER BY random() LIMIT 1) AS receptionist_portrait
FROM dss.system AS s,
     LATERAL generate_series(1, dss.pick_number(length(s.name)-1, 8)) AS p(n),
     LATERAL (SELECT r.name 
              FROM dss.resource AS r
              WHERE s.faction = r.faction AND
                    p.n = p.n -- Force evaluation for every planet
              ORDER BY random() LIMIT 1) mt(mine_type);

\echo 'Generating resource stocks...'

-- TODO: Base stock and use on faction (more for same faction)
INSERT INTO dss.resource_stock
SELECT
  p.name AS name,
  p.system AS system,
  r.name AS resource,
  round(30*random()) AS stock,
  round(5*random())*floor(2*random()) AS use -- 50% chance of use=0
FROM dss.planet AS p, dss.resource AS r;

-- Ships and Actors

-- First generate miners and traders

\echo 'Generating traders and miners...'

INSERT INTO dss.actor(aid, name, faction, is_player, money, cur_planet, cur_system,
                      load, load_type, load_max, warp_efficiency, fly_efficiency, mining_efficiency,
                      moved, acted, state)
SELECT
  (ARRAY['miner', 'trader'])[dss.pick_number(1, 2)] || (ROW_NUMBER() OVER ())::text AS aid,
  dss.pick_name(s.faction) AS name,
  s.faction AS faction,
  false AS is_player,
  50 + 200*random() AS money,
  p.name AS cur_planet,
  p.system AS cur_system,
  0 AS load,
  NULL AS load_type,
  round(5 + random()*5) AS max_load,
  0 AS warp_efficiency,
  1 + round(random() * 10) AS fly_efficiency,
  1 + round(random() * 20) AS mining_efficiency,
  false AS moved,
  false AS acted,
  NULL AS state
FROM dss.planet AS p
     JOIN dss.system AS s ON (p.system = s.name);

-- Generate warp traders

\echo 'Generating warp traders...'

INSERT INTO dss.actor(aid, name, faction, is_player, money, cur_planet, cur_system,
                      load, load_type, load_max, warp_efficiency, fly_efficiency, mining_efficiency,
                      moved, acted, state)
SELECT
  'warp_trader' || (ROW_NUMBER() OVER ())::text,
  dss.pick_name(s.faction) AS name,
  s.faction,
  false AS is_player,
  500 + 300*random() AS money, -- More money for warp traders!
  (SELECT name FROM dss.planet WHERE system = s.name ORDER BY random() LIMIT 1) AS cur_planet,
  s.name AS cur_system,
  0 AS load,
  NULL AS load_type,
  5*round(5 + random()*5) AS max_load, -- More load for warp traders!
  1 + round(random() * 20) AS warp_efficiency,
  1 + round(random() * 10) AS fly_efficiency,
  0 AS mining_efficiency,
  false AS moved,
  false AS acted,
  NULL AS state
FROM dss.system AS s
WHERE random() < 0.5;

\echo 'Updating stats according to faction...'

UPDATE dss.actor
SET mining_efficiency = mining_efficiency + 5
WHERE faction = 'sznerckk';

UPDATE dss.actor
SET warp_efficiency = warp_efficiency + 5
WHERE faction = 'ooulium';

UPDATE dss.actor
SET fly_efficiency = fly_efficiency + 5
WHERE faction = 'carbonae';

UPDATE dss.actor
SET load_max = load_max + 5
WHERE faction = 'sonos';

DO $$DECLARE a text;
BEGIN
    FOR a IN SELECT aid FROM dss.actor
    LOOP
        PERFORM create_actor_user(a);
    END LOOP;
END$$;

-- Add tick_commands for AI

INSERT INTO dss.tick_commands
SELECT a.aid, s.name || s.n::text, s.n, s.command
FROM dss.actor AS a, dss.script AS s
WHERE starts_with(a.aid, s.name);

-- Simulate 10 ticks for random parts of actors

\echo 'Simulating some history...'

SELECT dss.simulate_random(n, 20) AS "Simulated turns:"
FROM (SELECT count(*)/10 FROM dss.actor) AS t(n);

\echo 'Done generating universe!'
