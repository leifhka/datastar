CREATE FUNCTION faction()
RETURNS TABLE(fid int, name text, description text) AS
$body$
    SELECT fid, name, description FROM dss.faction;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW faction AS
SELECT * FROM faction();

CREATE FUNCTION psystems()
RETURNS TABLE(name text, faction text, pos float[], distance float, warp_price float) AS
$body$
    SELECT t.name, t.faction, t.pos,
           dss.distance(t.pos, f.pos) AS distance,
           dss.warp_price(f.name, t.name, a.warp_efficiency, a.load) AS warp_price
    FROM dss.system AS t, dss.system AS f, dss.actor AS a
    WHERE f.name = a.cur_system AND a.aid = session_user;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW systems AS
SELECT * FROM psystems();

CREATE FUNCTION psystem()
RETURNS TABLE(name text, faction text, num_planets bigint, num_actors bigint) AS
$body$
    SELECT s.name, s.faction,
      (SELECT count(*) FROM dss.planet AS p WHERE p.system = s.name) AS num_planets,
      (SELECT count(*) FROM dss.actor AS a WHERE a.cur_system = s.name) AS num_actors
    FROM dss.system AS s
    WHERE name = (SELECT cur_system FROM dss.actor WHERE aid = session_user);
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW system AS
SELECT * FROM psystem();

CREATE FUNCTION planets()
RETURNS TABLE(id int, name text, symbol text, pos float[], distance float, fly_price float) AS
$body$
    SELECT t.id, t.name, t.symbol,
           t.pos,
           dss.distance(t.pos, f.pos) AS distance,
           dss.fly_price(system, f.name, t.name, a.fly_efficiency, a.load) AS fly_price
    FROM dss.planet AS t JOIN dss.planet AS f USING (system), dss.actor AS a
    WHERE system = a.cur_system AND f.name = a.cur_planet AND a.aid = session_user;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW planets AS
SELECT * FROM planets();

CREATE FUNCTION planet()
RETURNS TABLE(id int, name text, faction text, mine_type text, mine_stock text, provides_updates text, upgrade_price float, receptionist_name text, receptionist_portrait text) AS
$body$
    SELECT p.id,
      p.name,
      s.faction,
      p.mine_type, 
      CASE WHEN p.mine_stock > 10000 THEN 'rich'
           WHEN p.mine_stock > 1000 THEN 'poor'
           WHEN p.mine_stock = 0 THEN 'empty'
           ELSE 'almost empty'
      END AS mine_stock,
      CASE WHEN p.provides_upgrades THEN f.upgrade ELSE NULL END AS provides_upgrades,
      CASE WHEN p.provides_upgrades THEN dss.upgrade_price(p.system, p.name) ELSE NULL END AS upgrade_price,
      p.receptionist_name,
      p.receptionist_portrait
    FROM dss.planet AS p
         JOIN dss.system AS s ON (p.system = s.name)
         JOIN dss.actor AS a ON (a.cur_planet = p.name AND a.cur_system = p.system)
         JOIN dss.faction AS f ON (s.faction = f.name)
    WHERE a.aid = session_user;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW planet AS
SELECT * FROM planet();

CREATE FUNCTION market()
RETURNS TABLE(id int, resource text, sell_price float, buy_price float) AS
$body$
    SELECT p.id,
      p.resource,
      p.sell_price,
      p.buy_price
    FROM dss.resource_price AS p
         JOIN dss.system AS s ON (p.system = s.name)
         JOIN dss.actor AS a ON (a.cur_planet = p.planet AND a.cur_system = p.system)
    WHERE a.aid = session_user;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW market AS
SELECT * FROM market();

CREATE FUNCTION actors()
RETURNS TABLE(aid text, name text, faction text) AS
$body$
    SELECT o.aid, o.name, o.faction
    FROM dss.actor AS o 
         JOIN dss.actor AS a USING (cur_planet, cur_system)
    WHERE a.aid = session_user;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW actors AS
SELECT * FROM actors();

CREATE FUNCTION status()
RETURNS TABLE(name text, faction text, money float, cur_planet text, cur_system text, load int, load_type text, load_max int, warp_efficiency int, fly_efficiency int, mining_efficiency int, moved boolean, acted boolean, state text, money_rating float) AS
$body$
    SELECT name, faction, money,
      cur_planet, cur_system,
      load, load_type, load_max,
      warp_efficiency, fly_efficiency, mining_efficiency,
      moved, acted, state,
      dss.money_rating(p.money) AS money_rating
    FROM dss.actor AS p
    WHERE aid = session_user;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW status AS
SELECT * FROM status();

CREATE FUNCTION tick_commands()
RETURNS TABLE(name text, n int, command text) AS
$body$
    SELECT name, n, command
    FROM dss.tick_commands
    WHERE aid = session_user;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW tick_commands AS
SELECT * FROM tick_commands();

-- View for drawing current solar system
CREATE VIEW current_system(current_system) AS
WITH
  bounds AS (
    SELECT max(pos[1]) AS max_x,
           min(pos[1]) AS min_x,
           max(pos[2]) AS max_y,
           min(pos[2]) AS min_y
      FROM planets
  ),
  steps AS (
    SELECT (max_x - min_x)/30 AS sx,
           (max_y - min_y)/15 AS sy
      FROM bounds
  ),
  points AS (
    SELECT dx, dy, name, pos, symbol
      FROM planets AS p,
           generate_series(0, 30) AS v1(dx),
           generate_series(0, 15) AS v2(dy),
           bounds,
           steps
     WHERE abs(pos[1] - (min_x + dx*sx)) <= sx/2.0 AND
           abs(pos[2] - (min_y + dy*sy)) <= sy/2.0
  ),
  lines AS (
    SELECT y, x,
           CASE -- TODO: Ensure no collisions 
             WHEN 1 = (SELECT count(*) FROM points WHERE dx = x AND dy = y)
                 THEN (SELECT symbol FROM points WHERE dx = x AND dy = y)
             WHEN 2 <= (SELECT count(*) FROM points WHERE dx = x AND dy = y)
                 THEN '8'
             WHEN x = 15 AND y = 7
                 THEN '@'
             ELSE ' '
           END AS sign
      FROM generate_series(0, 30) AS t1(x),
           generate_series(0, 15) AS t2(y)
  ),
  map AS (
      SELECT y+1 AS n, -- start counting at 1
             string_agg(sign, '' ORDER BY x) AS line
        FROM lines
    GROUP BY y
  ),
  nplanets AS (
     SELECT row_number() OVER () AS n, *
       FROM planets
  )
SELECT line, id, symbol, name, distance, fly_price
  FROM map LEFT JOIN nplanets USING (n);

CREATE FUNCTION current_tick()
RETURNS TABLE(tick int) AS
$body$
    SELECT last_value FROM dss.tick;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW current_tick AS
SELECT * FROM current_tick();

CREATE FUNCTION see_msgs()
RETURNS TABLE(aid text, tick int, msg text, command_cost float) AS
$body$
    SELECT *
    FROM dss.system_message
    WHERE aid = session_user;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW system_message AS
SELECT * FROM see_msgs();

CREATE FUNCTION see_chat()
RETURNS TABLE(aid text, msg text, posted timestamp) AS
$body$
    SELECT * FROM dss.chat;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW chat AS
SELECT * FROM see_chat();

CREATE FUNCTION highscore()
RETURNS TABLE(place int, aid text, money float, money_rating float) AS
$body$
    SELECT row_number() OVER () AS place, aid, money, dss.money_rating(money)
    FROM dss.actor
    WHERE is_player
    ORDER BY money_rating DESC;
$body$ LANGUAGE SQL SECURITY DEFINER;

CREATE VIEW highscore AS
SELECT * FROM highscore();

CREATE VIEW game_functions(name, ftype, description)
AS SELECT * FROM (VALUES
    ('create_player(fid int)', null, 'Creates a player in the game for the user with the faction given by the argument fid (to see information about each faction, execute SELECT * FROM faction;). This can only be executed once.'),
    ('constant(constant_name text)', 'Info', 'Returns the value of the system constant, such as ''vat'', ''warp_price_mode'' and ''fly_price_mod'''),
    ('set_state(new_state text)', null, 'Updates the state value in the status-table (mostly used by bots for scripts/tick-commands, but player can also use this for convenience).'),
    ('player_aid()', 'Info', 'Returns the aid-value of the current player.'),
    ('tick()', null, 'Ends the turn. After tick is called, tick-queries are executed, all NPCs can act and move, and planets consume resources.'),
    ('warp(to_system text)', 'Move AND act', 'Warps the player to the argument system. Gives error if not enough money for warp or system does not exist.'),
    ('fly(to_planet int)', 'Move', 'Flies the player to the planet given by the argument id within the current system. Gives error if not enough money for flight or planet does not exist (in current system).'),
    ('mine()', 'Act', 'Mines the current planet for resources.'),
    ('sell(num int)', 'Act', 'Sells the argument number of resoures currently loaded on ship.'),
    ('sell_all()', 'Act', 'Sells the all of the resoures currently loaded on ship.'),
    ('buy(resource text, num int)', 'Act', 'Buys the argument number of the resoure with the argument resource name from market on current planet into ship''s load. Gives error if product does not exists, if resource to buy is not same as currently loaded resource (if any) or if the number of resources after buy exceeds capacity (max_load) of ship.'),
    ('buy(resource int, num int)', 'Act', 'Same as buy(text, int), except that the first agrument is the resource id instead of name.'),
    ('upgrade(num int)', 'Act', 'Buys the argument number of the upgrades provided by the current planet to player''s ship. Gives error if planet does not provide upgrades or if not enough money to buy the given number of upgrades.'),
    ('stay()', 'Move', 'Does nothing, but counts as a move-action (used in scripts).'),
    ('wait()', 'Act', 'Does nothing, but counts as an act-action (used in scripts).'),
    ('chat(text)', null, 'Adds the argument message to the game''s chat.'), 
    ('insert_tick_command(name text, n int, command text)', null, 'Creates a tick-command with name equal to first argument, that on each end of turn executes the argument commadn. The command can be any of INSERT, UPDATE, DELETE, TRUNCATE, etc. The commands are executed in the order of the second (n) argument.'), 
    ('delete_tick_command(name text)', null, 'Deletes the tick-query with the given argument as name.')) t;

CREATE VIEW game_tables(name, description) AS
SELECT * FROM (VALUES
    ('systems', 'Describes all systems in the universe.'),
    ('planets', 'Describes all planets in the current system (i.e. the system the player is currently in).'),
    ('planet', 'Describes the current planet (i.e. the planet the player is currently on) with potentil upgrades the player can buy for its ship.'),
    ('market', 'Lists all resources with buy and sell prices for the current planet.'),
    ('actors', 'Lists all actors (NPCs) currently on the current planet.'),
    ('status', 'Describes the status of the player (position, money, load, score, etc.).'),
    ('current_tick', 'Contains the integer value that denotes the current tick/turn.'),
    ('current_system', 'Similar to the table planets, but also gives an ASCII-map of the current system.'),
    ('tick_commands', 'Lists all active tick-commands made by the player.'),
    ('system_message', 'Lists all messages from the system to you, such as errors of executed tick-commands, and cost (if any) of your tick commands.'),
    ('chat', 'Contains all chat messages from all players.'),
    ('highscore', 'Shows the current highscore table for all players.'),
    ('game_functions', 'Describes all functions the player can use to act and move in the game.'),
    ('game_tables', 'This table.')) t;
