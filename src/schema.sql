\echo 'Setting up schema...'

CREATE SCHEMA dss;

--[[NOT GENERATED]]--

-- Resource categories
CREATE TABLE dss.category (
    name text PRIMARY KEY
);

INSERT INTO dss.category VALUES
('Ore'), ('Food'), ('Liquid'), ('Luxury'), ('Biological');

CREATE TABLE dss.faction (
    fid int UNIQUE NOT NULL,
    name text PRIMARY KEY,
    upgrade text NOT NULL,
    description text NOT NULL
);

INSERT INTO dss.faction VALUES
(1, 'sznerckk', 'mining efficiency', 'A bug-like faction with exceptional mining-efficiency.'),
(2, 'ooulium', 'warp_efficiency', 'A peacefull faction with highly advanced warp-drives.'),
(3, 'carbonae', 'fly_efficiency', 'A human-like faction with highly developed rocket engines.'),
(4, 'sonos', 'load_max', 'A robot faction with very large ships.');

CREATE TABLE dss.constants(
  name text PRIMARY KEY,
  value text NOT NULL
);

INSERT INTO dss.constants VALUES
('vat', '1.25'), -- value added tax
('upgrade_price_mod', '50.0'), -- upgrade price multiplier
('warp_price_mod', '0.05'), -- fuel price multiplier for warping
('fly_price_mod', '5'), -- fuel price multiplier for flying
('warp_price', '100'), -- max fuel price for warping (used by NPCs as aprox)
('fly_price', '10'); -- max fuel price for flying (used by NPCs as aprox)

CREATE OR REPLACE FUNCTION constant(n text) RETURNS text AS
$body$
  SELECT value FROM dss.constants WHERE name = n;
$body$ language sql SECURITY DEFINER;

CREATE TABLE dss.planet_symbol(nr serial PRIMARY KEY, symbol text UNIQUE NOT NULL);
INSERT INTO dss.planet_symbol(symbol) VALUES
('.'), (','), ('o'), ('O'), ('0'),
('ø'), ('Ø'), ('¤'), ('*'), ('Q');

--[[GENERATED]]--

CREATE TABLE dss.resource (
  id SERIAL UNIQUE NOT NULL,
  name text PRIMARY KEY,
  faction text NOT NULL,
  category text REFERENCES dss.category(name),
  value float NOT NULL CHECK (value > 0)
);

CREATE INDEX ON dss.resource(id);
CREATE INDEX ON dss.resource(value);

CREATE TABLE dss.system (
  name text PRIMARY KEY,
  faction text NOT NULL,
  pos float[] NOT NULL 
);

-- Each planet also has a mine which one can
-- mine a particular resource from
CREATE TABLE dss.planet (
  id int NOT NULL,
  name text,
  system text REFERENCES dss.system(name),
  symbol text REFERENCES dss.planet_symbol(symbol),
  mine_type text REFERENCES dss.resource(name),
  mine_stock int NOT NULL CHECK (mine_stock >= 0),
  provides_upgrades boolean NOT NULL,
  pos float[] NOT NULL,
  receptionist_name text NOT NULL,
  receptionist_portrait text NOT NULL,
  PRIMARY KEY (name, system),
  UNIQUE (id, system)
);

CREATE INDEX ON dss.planet(id);
CREATE INDEX ON dss.planet(name);
CREATE INDEX ON dss.planet(system);
CREATE INDEX ON dss.planet(mine_type);

-- Number of a given resource a particular system needs/wants
-- The price the planet is willing to pay is derived
-- from this value (see resource_price below)
CREATE TABLE dss.resource_stock (
  planet text NOT NULL,
  system text NOT NULL,
  resource text REFERENCES dss.resource(name),
  stock int NOT NULL CHECK (stock >= 0),
  use int NOT NULL CHECK (use >= 0),
  FOREIGN KEY (planet, system) REFERENCES dss.planet(name, system),
  PRIMARY KEY (planet, system, resource)
);

CREATE INDEX ON dss.resource_stock(planet);
CREATE INDEX ON dss.resource_stock(system);
CREATE INDEX ON dss.resource_stock(resource);

CREATE VIEW dss.resource_price AS
SELECT 
  id,
  planet,
  system,
  resource,
  stock,
  sqrt(10*use::float / greatest(stock, 1) )*value AS sell_price,
  sqrt(greatest(10*use, 1)::float  / greatest(stock, 1))*value*constant('vat')::float AS buy_price
FROM dss.resource AS r
     JOIN dss.resource_stock AS rs ON (rs.resource = r.name);

CREATE TABLE dss.actor (
  aid text PRIMARY KEY,
  name text NOT NULL UNIQUE,
  faction text NOT NULL CHECK (faction IN ('sznerckk', 'ooulium', 'sonos', 'carbonae')),
  is_player boolean NOT NULL, -- True for actual player (false for NPCs)
  money float NOT NULL CHECK (money >= 0),
  cur_planet text NOT NULL,
  cur_system text NOT NULL,
  load int CHECK (load >= 0 AND load <= load_max),
  load_type text REFERENCES dss.resource(name),
  load_max int NOT NULL CHECK (load_max >= 0),
  fly_efficiency int NOT NULL CHECK (fly_efficiency >= 0),
  warp_efficiency int NOT NULL CHECK (warp_efficiency >= 0),
  mining_efficiency int NOT NULL CHECK (mining_efficiency >= 0),
  moved boolean NOT NULL,
  acted boolean NOT NULL,
  state text, -- Used by script for keeping state
  FOREIGN KEY (cur_planet, cur_system) REFERENCES dss.planet(name, system)
);

CREATE INDEX ON dss.actor(cur_system, cur_planet);
CREATE INDEX ON dss.actor(name);

CREATE VIEW dss.actor_context AS
SELECT aid, a.name, a.faction, a.is_player, a.money, 
       a.cur_planet, a.cur_system, a.load, a.load_type, a.load_max,
       a.fly_efficiency, a.warp_efficiency, a.mining_efficiency,
       a.moved, a.acted, a.state,
       p.mine_type, p.mine_stock,
       sy.faction AS system_faction
FROM dss.actor AS a
     JOIN dss.planet AS p ON (a.cur_planet = p.name AND a.cur_system = p.system)
     JOIN dss.system AS sy ON (a.cur_system = sy.name);

-- Chats and messages

CREATE TABLE dss.system_message(aid text, tick int, msg text, command_cost float);

CREATE TABLE dss.chat(aid text, msg text, posted timestamp);

-- Scripts

CREATE OR REPLACE FUNCTION dss.set_state(actor_id text, new_state text) RETURNS void AS
$body$
  UPDATE dss.actor
  SET state = new_state
  WHERE aid = actor_id;
$body$ language sql;

-- Used to generate tick_commands for (non-player) actors
CREATE TABLE dss.script (
  name text,
  n int NOT NULL,
  command text NOT NULL,
  PRIMARY KEY (name, n)
);

INSERT INTO dss.script VALUES
-- miner script
('miner', 1, --mine
 $command$
   SELECT 
     CASE
       WHEN mine() LIKE '% 0 %' THEN set_state('sell') -- mine empty
     END AS mine,
     stay()
   FROM status
   WHERE state = 'mine' AND load < load_max;
 $command$),
('miner', 2, -- wait
 $command$
   SELECT wait(), stay()
   FROM status
   WHERE NOT acted AND
         money < constant('fly_price')::float;
 $command$),
('miner', 3, -- fly and sell
 $command$
   SELECT fly((SELECT rp.planet
               FROM dss.resource_price AS rp
               WHERE cur_system = system
               ORDER BY rp.sell_price DESC LIMIT 1)),
          sell_all(),
          set_state('fly_mine')
   FROM status
   WHERE NOT acted AND
         (state = 'sell' OR
          (state = 'mine' AND
           load = load_max))
 $command$),
('miner', 4, -- fly and mine
 $command$
   SELECT fly((SELECT p.name
               FROM dss.planet AS p
                    JOIN dss.resource_price AS r
                    ON (p.system = r.system AND p.mine_type = r.resource)
               WHERE cur_system = p.system AND
                     p.mine_stock > 0
               ORDER BY round(log(greatest(1, r.sell_price))) DESC, random()
               LIMIT 1)),
          set_state('mine'),
          mine()
   FROM status 
   WHERE NOT acted AND
         state = 'fly_mine';
 $command$),
('miner', 5, -- init
 $command$
   SELECT set_state('fly_mine')
   FROM status
   WHERE NOT acted;
 $command$),
-- trader scripts
('trader', 1, --wait
 $command$
   SELECT stay(), wait()
   FROM status
   WHERE money < constant('fly_price')::float;
 $command$),
('trader', 2, -- sell
 $command$
   SELECT fly((SELECT rp.planet
               FROM dss.resource_price AS rp
               WHERE rp.system = cur_system AND rp.resource = load_type
               ORDER BY sell_price DESC LIMIT 1)),
          sell_all(),
          set_state('buy')
   FROM status
   WHERE NOT acted AND
         state = 'sell';
 $command$),
('trader', 3, -- buy
 $command$
   SELECT fly(planet),
          buy(resource,
              least(stock,
                    floor(greatest(0, (money - constant('fly_price')::float))/buy_price),
                    load_max)::int),
          set_state('sell')
   FROM status,
        LATERAL (SELECT b.planet, b.buy_price, b.stock, resource
                 FROM dss.resource_price AS b
                      JOIN dss.resource_price AS s USING (system, resource)
                 WHERE cur_system = system
                 ORDER BY round((s.sell_price - b.buy_price)) DESC, random()
                 LIMIT 1) AS t
   WHERE NOT acted AND
         state = 'buy';
 $command$),
('trader', 4, -- init
 $command$
   SELECT set_state('buy')
   FROM status
   WHERE NOT acted;
 $command$),
-- warp traders
('warp_trader', 1, -- wait
 $command$
   SELECT stay(), wait()
   FROM status
   WHERE state != 'buy' AND
         money < constant('warp_price')::float;
 $command$),
('warp_trader', 2, -- buy
 $command$
   SELECT fly(planet),
          buy(resource, 
              least(stock,
                    floor(greatest(0, (money - 3*constant('warp_price')::float))/buy_price),
                    load_max)::int),
          set_state('warp')
    FROM status,
         LATERAL (SELECT b.planet, resource, b.stock, b.buy_price
                  FROM dss.resource_price AS b JOIN dss.resource_price AS s USING (resource)
                  WHERE cur_system = b.system AND
                        cur_system != s.system AND
                        b.stock > 0
                  ORDER BY s.sell_price - b.buy_price DESC, random()
                  LIMIT 1) AS t
    WHERE NOT acted AND
          state = 'buy';
 $command$),
('warp_trader', 3, -- warp
 $command$
   SELECT warp((SELECT system
                FROM dss.resource_price
                WHERE resource = status.load_type AND
                      system != status.cur_system
                ORDER BY sell_price DESC, random() LIMIT 1)),
          set_state('sell')
   FROM status
   WHERE NOT acted AND
         state = 'warp';
 $command$),
('warp_trader', 4, -- sell
 $command$
   SELECT fly((SELECT s.planet
               FROM dss.resource_price AS s
               WHERE cur_system = s.system AND load_type = s.resource
               ORDER BY s.sell_price DESC, random()
               LIMIT 1)),
          sell_all(),
          set_state('buy')
   FROM status
   WHERE NOT acted AND
         state = 'sell';
 $command$),
('warp_trader', 5, -- init
 $command$
   SELECT set_state('buy')
   FROM status
   WHERE NOT acted;
 $command$);

CREATE SEQUENCE dss.tick;
SELECT nextval('dss.tick'); -- Init turn to 1

-- User tick-queries

CREATE TABLE dss.tick_commands(
    aid text,
    name text,
    n int NOT NULL CHECK (n >= 1 AND n <= 10),
    command text NOT NULL,
    PRIMARY KEY (aid, name),
    UNIQUE (aid, n)
); 

CREATE OR REPLACE FUNCTION dss.exec_tick_command(username text, cname text, command text)
RETURNS void AS
$body$
DECLARE
  start_time timestamp;
  end_time timestamp;
  command_time float;
  command_cost_base float;
  command_cost float;
  errmsg text;
BEGIN
  -- Set session_user = username
  EXECUTE format('SET SESSION AUTHORIZATION %I;', username);

  start_time := clock_timestamp();
  EXECUTE command;
  end_time := clock_timestamp();
  
  -- Reset session_user = <superuser>
  RESET SESSION AUTHORIZATION;

  command_time := extract(epoch from (end_time - start_time)); -- in sec.
  command_cost_base := greatest(0, command_time - 0.1);
  IF command_cost_base > 0 THEN

    WITH
      u(cc) AS (
        UPDATE dss.actor
        SET money = greatest(0, -- never negative
                      money
                      - (command_cost_base
                         + least(money/10, -- never more than 10%
                                 pow(command_cost_base + money*command_cost_base/200, 1.3))))
        WHERE aid = username
        -- Return computed cost and return
        RETURNING (SELECT money FROM dss.actor WHERE aid = username) - money AS command_cost
      )
    SELECT cc INTO command_cost
    FROM u;

    PERFORM dss.post_system_message(username,
        format('Tick command with name %s executed in %s ms and cost %s money.',
            cname, (1000 * command_time)::text, command_cost::text),
            command_cost);
  END IF;

EXCEPTION WHEN OTHERS THEN
  GET STACKED DIAGNOSTICS errmsg = MESSAGE_TEXT;
  PERFORM dss.post_system_message(username,
      format('Tick command with name %s failed with error message: %s.',
             cname, errmsg, NULL));
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.exec_tick_commands_for(username text) RETURNS void AS
$body$
DECLARE
  command_r record;
BEGIN
    -- Execute each of the users command in order
    FOR command_r IN (
      SELECT *
      FROM dss.tick_commands
      WHERE aid = username ORDER BY n
    ) LOOP
        PERFORM dss.exec_tick_command(username, command_r.name, command_r.command);
    END LOOP;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.exec_tick_commands() RETURNS void AS
$body$
DECLARE
  username text;
  command_r record;
BEGIN
  -- Randomize order of users for fairness
  FOR username IN (
    SELECT aid
    FROM (SELECT DISTINCT aid FROM dss.tick_commands) t
    ORDER BY random()
  ) LOOP

    PERFORM dss.exec_tick_commands_for(username);
  END LOOP;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.post_system_message(username text, msg text, command_cost float)
RETURNS void AS
$body$
  INSERT INTO dss.system_message
  SELECT username, last_value AS tick, msg, command_cost
  FROM dss.tick;
$body$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION dss.distance(pos1 float[], pos2 float[]) RETURNS float AS
$body$
  SELECT sqrt(pow( (pos1[1] - pos2[1]) , 2) + pow( (pos1[2] - pos2[2]) , 2));
$body$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION dss.warp_price(fsystem text, tsystem text, warp_efficiency int, load int) RETURNS float AS
$body$
  SELECT (constant('warp_price_mod')::float * sqrt(1.0 + (load/100.0)) * dss.distance(fsy.pos, tsy.pos))/(1 + (warp_efficiency/10.0))
  FROM dss.system AS fsy, dss.system AS tsy
  WHERE fsy.name = fsystem AND tsy.name = tsystem;
$body$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION dss.fly_price(csystem text, fplanet text, tplanet text, fly_efficiency int, load int)
RETURNS float AS
$body$
  SELECT constant('fly_price_mod')::float * sqrt(1.0 + (load/100.0)) * dss.distance(fpl.pos, tpl.pos)/(1 + (fly_efficiency/10.0))
  FROM dss.planet AS fpl JOIN dss.planet AS tpl USING (system)
  WHERE system = csystem AND fpl.name = fplanet AND tpl.name = tplanet;
$body$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION dss.upgrade_price(usystem text, uplanet text)
RETURNS float AS
$body$
  SELECT constant('upgrade_price_mod')::float*(1 + avg(r.buy_price))
  FROM dss.planet AS p JOIN dss.resource_price AS r ON (p.system = r.system AND p.name = r.planet)
  WHERE p.system = usystem AND p.name = uplanet;
$body$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION dss.money_rating(pmoney float)
RETURNS float AS
$body$
  SELECT pmoney/avg(money)
  FROM dss.actor;
$body$ LANGUAGE sql;
