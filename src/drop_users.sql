CREATE OR REPLACE FUNCTION drop_user(username text)
RETURNS void AS
$body$
BEGIN
    -- Drop player's schema for own tables/views, but only
    -- if exists
    IF EXISTS (SELECT 1 FROM pg_roles WHERE rolname = username::name) THEN
        EXECUTE format('DROP OWNED BY %I CASCADE;', username);
        EXECUTE format('DROP ROLE %I;', username);
    END IF;
END;
$body$ LANGUAGE plpgsql;
