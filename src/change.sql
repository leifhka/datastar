-- Functions and triggers for changes to be applied at each tick
-- E.g. planets use resource, ships move, ships mine, etc.

CREATE OR REPLACE FUNCTION dss.acted(actor_id text) RETURNS void AS
$body$
BEGIN
  -- Check not acted
  IF (SELECT acted FROM dss.actor WHERE aid = actor_id) THEN
    RAISE EXCEPTION 'Actor % already acted this tick!', actor_id;
    RETURN;
  END IF;

  UPDATE dss.actor
  SET acted = true
  WHERE aid = actor_id;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.moved(actor_id text) RETURNS void AS
$body$
BEGIN
  -- Check not moved
  IF (SELECT moved FROM dss.actor WHERE aid = actor_id) THEN
    RAISE EXCEPTION 'Actor % already moved this tick!', actor_id;
    RETURN;
  END IF;

  UPDATE dss.actor
  SET moved = true
  WHERE aid = actor_id;
END;
$body$ LANGUAGE plpgsql;

CREATE TYPE dss.warp_info AS (
  price float,
  to_planet text
);

CREATE OR REPLACE FUNCTION dss.warp(actor_id text, csystem text) RETURNS dss.warp_info AS
$body$
DECLARE
  wprice float;
  a dss.actor;
BEGIN
  -- Check not moved nor acted
  IF (SELECT moved OR acted FROM dss.actor WHERE aid = actor_id) THEN
    RAISE EXCEPTION 'Actor % attempted to warp but has already moved or acted this tick!', actor_id;
    RETURN 0;
  END IF;

  -- Check that system exists
  IF NOT EXISTS (SELECT true FROM dss.system WHERE name = csystem) THEN
    RAISE EXCEPTION 'System % does not exist!', csystem;
    RETURN ROW(0, (SELECT cur_planet FROM dss.actor WHERE aid = actor_id));
  END IF;

  a := (SELECT ac.*::record FROM dss.actor AS ac WHERE aid = actor_id);
  wprice := dss.warp_price(a.cur_system, csystem, a.warp_efficiency, a.load);

  -- Check enough money for warping
  IF (wprice > a.money) THEN
    RAISE EXCEPTION 'Actor % has insufficient money for warping!', actor_id;
    RETURN ROW(0, (SELECT cur_planet FROM dss.actor WHERE aid = actor_id));
  END IF;

  UPDATE dss.actor
  SET cur_planet = (SELECT name FROM dss.planet AS p -- pick random planet in system
                    WHERE p.system = csystem
                    ORDER BY random() LIMIT 1),
      cur_system = csystem,
      money = money - wprice
  WHERE aid = actor_id;

  PERFORM dss.moved(actor_id);
  PERFORM dss.acted(actor_id);

  RETURN ROW(wprice, (SELECT cur_planet FROM dss.actor WHERE aid = actor_id));
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.fly(actor_id text, cplanet text) RETURNS float AS
$body$
DECLARE
  fprice float;
  a dss.actor;
BEGIN
  a := (SELECT ac.*::record FROM dss.actor AS ac WHERE aid = actor_id);

  -- Check not moved
  IF (SELECT moved FROM dss.actor WHERE aid = actor_id) THEN
    RAISE EXCEPTION 'Actor % attempted to fly but has already moved this tick!', actor_id;
    RETURN 0;
  END IF;

  -- Check that planet is in current system
  IF NOT EXISTS (SELECT true FROM dss.planet
                 WHERE name = cplanet AND
                       system = a.cur_system) THEN
    RAISE EXCEPTION 'Actor % attempted to fly to %, but planet not in current system!', actor_id, cplanet;
    RETURN 0;
  END IF;

  fprice := dss.fly_price(a.cur_system, a.cur_planet, cplanet, a.fly_efficiency, a.load);

  -- Check enough money for flying
  IF (fprice > a.money) THEN
    RAISE EXCEPTION 'Actor % attempted to fly, but has insufficient money for flying!', actor_id;
    RETURN 0;
  END IF;

  UPDATE dss.actor
  SET cur_planet = cplanet,
      money = money - fprice
  WHERE aid = actor_id;

  PERFORM dss.moved(actor_id);

  RETURN fprice;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.mine(actor_id text) RETURNS int AS
$body$
DECLARE
  to_mine int;
  a dss.actor;
BEGIN

  -- Check not acted
  IF (SELECT acted FROM dss.actor WHERE aid = actor_id) THEN
    RAISE EXCEPTION 'Actor % attempted to mine but has already acted this tick!', actor_id;
    RETURN 0;
  END IF;

  a := (SELECT ac.*::record FROM dss.actor AS ac WHERE aid = actor_id);

  -- Check enough space for minig
  IF (a.load_max <= a.load) THEN
    RAISE EXCEPTION 'Actor % attempted to mine with insufficient space on ship!', actor_id;
    RETURN 0;
  END IF;

  -- Check correct current load type for minig
  IF  (a.load_type IS NOT NULL AND
       a.load_type != (SELECT mine_type FROM dss.planet
                       WHERE name = a.cur_planet AND system = a.cur_system)) THEN
    RAISE EXCEPTION 'Actor % attempted to mine different type than already in load. Can only have one type of load!', actor_id;
    RETURN 0;
  END IF;

  to_mine := (
    SELECT least(greatest(0, a.load_max - a.load),
                 mine_stock,
                 floor(2 + random()*6*(1 + a.mining_efficiency/10.0)))
    FROM dss.planet
    WHERE a.cur_planet = name AND a.cur_system = system
  );

  UPDATE dss.planet
  SET mine_stock = mine_stock - to_mine
  WHERE name = a.cur_planet AND system = a.cur_system;

  UPDATE dss.actor
  SET load = load + to_mine,
      load_type = (SELECT mine_type
                   FROM dss.planet AS p
                   WHERE p.name = cur_planet AND p.system = cur_system)
  WHERE aid = actor_id;

  PERFORM dss.acted(actor_id);

  RETURN to_mine;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.sell_all(actor_id text) RETURNS float AS
$body$
BEGIN
  RETURN dss.sell(actor_id, (SELECT load FROM dss.actor WHERE aid = actor_id));
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.sell(actor_id text, n int) RETURNS float AS
$body$
DECLARE
  price float;
  a dss.actor;
BEGIN
  -- Check not acted
  IF (SELECT acted FROM dss.actor WHERE aid = actor_id) THEN
    RAISE EXCEPTION 'Actor % attempted to sell but has already acted this tick!', actor_id;
    RETURN 0;
  END IF;

  a := (SELECT ac.*::record FROM dss.actor AS ac WHERE aid = actor_id);
  -- Check that has load_type
  IF (a.load_type IS NULL) THEN
    RAISE EXCEPTION 'Actor % attempted to sell, but has nothing on ship!', actor_id;
    RETURN 0;
  END IF;

  -- Check that n <= load
  IF (n > a.load) THEN
    RAISE EXCEPTION 'Actor % attempted to sell % but only owns %!', actor_id, n, a.load;
    RETURN 0;
  END IF;

  SELECT sell_price INTO price
  FROM dss.resource_price
  WHERE planet = a.cur_planet AND
        system = a.cur_system AND
        resource = a.load_type;

  UPDATE dss.actor
  SET load = load - n,
      load_type = CASE WHEN load = n THEN NULL ELSE load_type END,
      money = money + n*price
  WHERE aid = actor_id;

  UPDATE dss.resource_stock
  SET stock = stock + n
  WHERE planet = a.cur_planet AND system = a.cur_system;

  PERFORM dss.acted(actor_id);
  RETURN price*n;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.buy(actor_id text, res text, n int) RETURNS float AS
$body$
DECLARE
  a dss.actor;
  price float;
  total_price float;
BEGIN
  -- Check not acted
  IF (SELECT acted FROM dss.actor WHERE aid = actor_id) THEN
    RAISE EXCEPTION 'Actor % attempted to buy but has already acted this tick!', actor_id;
    RETURN 0;
  END IF;

  a := (SELECT ac.*::record FROM dss.actor AS ac WHERE aid = actor_id);
  price := (SELECT buy_price FROM dss.resource_price
            WHERE resource = res AND
                  planet = a.cur_planet AND
                  system = a.cur_system);
  total_price := n * price;
  
  -- Check that product exists
  IF (price IS NULL) THEN
    RAISE EXCEPTION 'Actor % attempted to buy resource %, but it does not exist!', actor_id, res;
    RETURN 0;
  END IF;

  -- Check that has no load_type or same as product to buy
  IF (a.load_type IS NOT NULL AND a.load_type != res) THEN
    RAISE EXCEPTION 'Actor % attempted to buy resource, but has different resource loaded. Can only load one type of resource!', actor_id;
    RETURN 0;
  END IF;

  -- Check that load_max - load >= n
  IF (n > a.load_max - a.load) THEN
    RAISE EXCEPTION 'Actor % attempted to buy more resources than there is room for on ship!', actor_id;
    RETURN 0;
  END IF;

  -- Check enough money
  IF (a.money < total_price) THEN
    RAISE EXCEPTION 'Actor % attempted to buy resources for more money than actor currenty has.', actor_id;
    RETURN 0;
  END IF;

  UPDATE dss.actor
  SET money = money - total_price
  WHERE aid = actor_id;

  UPDATE dss.resource_stock
  SET stock = stock - n
  WHERE planet = a.cur_planet AND
        system = a.cur_system AND
        resource = a.load_type;

  UPDATE dss.actor
  SET load = load + n,
      load_type = CASE WHEN load + n > 0 THEN res ELSE NULL END
  WHERE aid = actor_id;

  PERFORM dss.acted(actor_id);
  RETURN total_price;
END;
$body$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION dss.consume() RETURNS void AS
$body$
  UPDATE dss.resource_stock
  SET stock = greatest(stock - use, 0);
$body$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION wait() RETURNS void AS
$body$
BEGIN
  IF (SELECT acted FROM dss.actor WHERE aid = session_user) THEN
    RAISE EXCEPTION 'Actor % attempted to wait but has already acted this tick!', session_user;
    RETURN;
  END IF;
  PERFORM dss.acted(session_user);
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION stay() RETURNS void AS
$body$
BEGIN
  IF (SELECT moved FROM dss.actor WHERE aid = session_user) THEN
    RAISE EXCEPTION 'Actor % attempted to stay but has already moved this tick!', session_user;
    RETURN;
  END IF;
  PERFORM dss.moved(session_user);
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION tick() RETURNS int AS
$body$
DECLARE
  tick int;
BEGIN
  UPDATE dss.actor
  SET moved = false,
      acted = false;

  PERFORM dss.consume();

  SELECT nextval('dss.tick') into tick;

  PERFORM dss.exec_tick_commands();

  RETURN tick;
END;
$body$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION dss.simulate_random_once(num_actors bigint) RETURNS void AS
$body$
DECLARE
  username text;
  command_r record;
BEGIN
  UPDATE dss.actor
  SET moved = false,
      acted = false;

  FOR username IN (
    SELECT aid
    FROM (SELECT DISTINCT aid FROM dss.tick_commands) t
    ORDER BY random()
    LIMIT num_actors
  ) LOOP

    PERFORM dss.exec_tick_commands_for(username);
  END LOOP;
END;
$body$ language plpgsql;

CREATE OR REPLACE FUNCTION dss.simulate_random(num_actors bigint, turns int) RETURNS bigint AS
$body$
BEGIN
  PERFORM dss.simulate_random_once(num_actors)
  FROM generate_series(1, turns) AS t;

  RETURN num_actors * turns;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION dss.player_aid() RETURNS text AS
$body$
  SELECT aid FROM dss.actor WHERE aid = session_user; -- ensure user is player
$body$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION dss.greet_warp(to_system text, to_planet text) RETURNS text AS
$body$
  SELECT p.receptionist_portrait
         || E'\n\nPlanet receptionist ' || p.receptionist_name || E':\n'
         || '"Welcome warper to the ' || s.faction || E'ian planet\n'
         || 'of ' || p.name || ' in the ' || s.name || ' system.'
         || CASE WHEN p.provides_upgrades
                 THEN E'\nThis planet provides upgrades for your\nship''s ' || f.upgrade || '!"'
                 ELSE '"'
            END
  FROM dss.planet AS p
       JOIN dss.system AS s ON (p.system = s.name)
       JOIN dss.faction AS f ON (s.faction = f.name)
  WHERE s.name = to_system AND p.name = to_planet;
$body$ LANGUAGE sql;

CREATE OR REPLACE FUNCTION dss.greet_fly(to_system text, to_planet text) RETURNS text AS
$body$
  SELECT p.receptionist_portrait
         || E'\n\nPlanet receptionist ' || p.receptionist_name || E':\n'
         || '"Welcome traveler to the planet ' || p.name || '.'
         || CASE WHEN p.provides_upgrades
                 THEN E'\nThis planet provides upgrades for your\nship''s ' || f.upgrade || '!"'
                 ELSE '"'
            END
  FROM dss.planet AS p
       JOIN dss.system AS s ON (p.system = s.name)
       JOIN dss.faction AS f ON (s.faction = f.name)
  WHERE p.system = to_system AND p.name = to_planet;
$body$ LANGUAGE sql;


--[[Player functions]]

CREATE OR REPLACE FUNCTION set_state(new_state text) RETURNS void AS
$body$
  SELECT dss.set_state(session_user, new_state);
$body$ LANGUAGE sql SECURITY DEFINER;

-- Actions

CREATE OR REPLACE FUNCTION warp(to_system text) RETURNS text AS
$body$
DECLARE
  actor_id text;
  wi dss.warp_info;
BEGIN
  IF to_system IS NULL THEN
    RAISE EXCEPTION 'Actor % attempted to warp to system with argument NULL.', session_user;
    RETURN 'Warp aborted.';
  END IF;

  wi := dss.warp(session_user, to_system);
  RETURN format('*Warped to system %s and landed on planet %s, paid %s in fuel.*', to_system, wi.to_planet, round(wi.price::numeric, 2))
    || E'\n\n' || dss.greet_warp(to_system, wi.to_planet);
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION fly(to_planet text) RETURNS text AS
$body$
DECLARE
  actor_id text;
  pname text;
  fprice float;
BEGIN
  IF to_planet IS NULL THEN
    RAISE EXCEPTION 'Actor % attempted to fly to planet with argument NULL.', session_user;
    RETURN 'Fly aborted.';
  END IF;

  SELECT aid, t.name INTO actor_id, pname
  FROM dss.actor AS a JOIN dss.planet AS t ON (a.cur_system = t.system)
  WHERE a.aid = session_user AND (t.id::text = to_planet OR t.name = to_planet);

  fprice = dss.fly(actor_id, pname);
  RETURN format('*Landed on planet %s, paid %s in fuel.*', pname, round(fprice::numeric, 2))
    || E'\n\n'
    || dss.greet_fly((SELECT cur_system FROM dss.actor WHERE aid = actor_id), pname);
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION fly(to_planet int) RETURNS text AS
$body$
  SELECT fly(to_planet::text);
$body$ LANGUAGE sql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION mine() RETURNS text AS
$body$
DECLARE
  mined int;
BEGIN
  mined := dss.mine(dss.player_aid());
  RETURN format('*Mined %s of %s.*', mined, (SELECT load_type FROM dss.actor WHERE aid = session_user));
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION sell(n int) RETURNS text AS
$body$
DECLARE
  lt text;
  total_price float;
BEGIN
  lt := (SELECT load_type FROM dss.actor WHERE aid = session_user);
  total_price := dss.sell(dss.player_aid(), n);
  RETURN format('*Sold %s %s(s) for %s.*', n, lt, round(total_price::numeric, 2));
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION sell_all() RETURNS text AS
$body$
DECLARE
  lt text;
BEGIN
  RETURN sell((SELECT load FROM dss.actor WHERE aid = session_user));
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION buy(prodname text, n int) RETURNS text AS
$body$
DECLARE
  total_price float;
BEGIN
  total_price := dss.buy(dss.player_aid(), prodname, n);
  RETURN format('*Bought %s %s(s) for %s.*', n, prodname, round(total_price::numeric, 2));
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION buy(prod int, n int) RETURNS text AS
$body$
DECLARE
  prodname text;
  total_price float;
BEGIN
  SELECT name INTO prodname FROM dss.resource WHERE id = prod;
  RETURN buy(prodname, n);
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION upgrade(n int) RETURNS text AS
$body$
DECLARE
  total_price float;
  upgrade_type text;
BEGIN
  SELECT n*upgrade_price, provides_upgrades INTO total_price, upgrade_type FROM planet;

  ASSERT -- Check that planet can upgrade
    (total_price IS NOT NULL),
    'Planet does not provide upgrades!';

  ASSERT -- Check enough money
    (total_price <= (SELECT money FROM dss.actor WHERE aid = session_user)),
    'Not enough money for upgrade!';

  EXECUTE format('UPDATE dss.actor
                  SET money = money - %s,
                      %s = %s + %s
                  WHERE aid = session_user;',
                 total_price, upgrade_type, upgrade_type, n);

  PERFORM dss.acted(dss.player_aid());
  RETURN format('*Bought %s upgrades of %s.*', n, upgrade_type);
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION insert_tick_command(name text, n int, command text)
RETURNS void AS
$body$
  INSERT INTO dss.tick_commands VALUES (session_user, name, n, command);
$body$ LANGUAGE sql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION delete_tick_command(nm text)
RETURNS void AS
$body$
  DELETE FROM dss.tick_commands WHERE aid = session_user AND name = nm;
$body$ LANGUAGE sql SECURITY DEFINER;

CREATE OR REPLACE FUNCTION chat(msg text)
RETURNS VOID AS
$body$
  INSERT INTO dss.chat VALUES (session_user, msg, now());
$body$ LANGUAGE sql SECURITY DEFINER;
