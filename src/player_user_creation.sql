-- Player creation functions

CREATE FUNCTION create_player_user(username text, pwd text)
RETURNS boolean AS
$body$
BEGIN
    IF username IN (SELECT rolname FROM pg_roles) THEN
      RAISE WARNING 'User with this username % already exists!', username;
      RETURN false;
    END IF;

    EXECUTE format($$CREATE USER %I WITH PASSWORD '%s';$$, username, pwd);
    EXECUTE format('GRANT USAGE ON SCHEMA public TO %I;', username);
    --EXECUTE format('REVOKE ALL ON ALL FUNCTIONS IN SCHEMA public FROM %I;', username);

    -- Create player's schema for own tables/views
    EXECUTE format('CREATE SCHEMA %I;', username);
    EXECUTE format('GRANT ALL ON SCHEMA %I TO %I;', username, username);
    EXECUTE format('ALTER SCHEMA %I OWNER TO %I;', username, username);

    -- Crant usage of player schema
    EXECUTE format('GRANT SELECT ON
            faction, systems, system, planets, planet,
            market, actors, status, tick_commands,
            current_system, current_tick,
            chat, system_message, highscore,
            game_functions, game_tables
        TO %I;', username);

    -- Grant execute on underlying functions for views
    EXECUTE format('GRANT EXECUTE ON FUNCTION faction() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION psystems() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION psystem() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION planets() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION planet() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION market() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION actors() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION status() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION tick_commands() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION current_tick() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION see_msgs() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION see_chat() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION highscore() TO %I;', username);
    
    -- Grant usage of game functions
    EXECUTE format('GRANT EXECUTE ON FUNCTION set_state(text) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION constant(text) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION warp(text) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION fly(text) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION fly(int) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION mine() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION sell(int) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION sell_all() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION buy(text, int) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION buy(int, int) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION wait() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION stay() TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION upgrade(int) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION insert_tick_command(text, int, text) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION delete_tick_command(text) TO %I;', username);
    EXECUTE format('GRANT EXECUTE ON FUNCTION chat(text) TO %I;', username);

    RETURN true;
END;
$body$ LANGUAGE plpgsql;

CREATE FUNCTION generate_actor_pwd()
RETURNS text AS
$body$
    SELECT md5('pwd' || random()::text)::text;
$body$ LANGUAGE sql;

-- Used only for non-player actors
CREATE FUNCTION create_actor_user(aid text)
RETURNS boolean AS
$body$
BEGIN
    IF create_player_user(aid, generate_actor_pwd()) THEN
        -- Grant aditional privileges required by scripts
        EXECUTE format('GRANT USAGE ON SCHEMA dss TO %I;', aid);
        EXECUTE format('GRANT SELECT ON
            dss.planet, dss.system, dss.resource_price, dss.actor
        TO %I;', aid);

        RETURN true;
    END IF;
    RETURN false;
END;
$body$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION create_player(username text, pwd text, player_fid int)
RETURNS void AS
$body$
DECLARE
    faction_name text;
BEGIN

    IF NOT create_player_user(username, pwd) THEN
      RETURN;
    END IF;

    faction_name := (SELECT name FROM dss.faction WHERE fid = player_fid);

    IF (faction_name IS NULL) THEN
      RAISE WARNING 'Argument number % is not a valid fid of a faction!', player_fid;
      RETURN;
    END IF;

    INSERT INTO dss.actor(aid, name, faction, is_player, money, cur_planet, cur_system,
                          load, load_type, load_max, warp_efficiency, fly_efficiency, mining_efficiency,
                          moved, acted, state)
    WITH
      loc AS (
        SELECT p.name, p.system
        FROM dss.planet AS p JOIN dss.system AS s ON (p.system = s.name)
        WHERE s.faction = faction_name
        ORDER BY random() LIMIT 1
      )
    SELECT 
      username,
      username,
      faction_name,
      true,
      300,
      name AS cur_planet,
      system AS cur_system,
      0 AS load,
      NULL as load_type,
      CASE WHEN faction_name = 'sonos' THEN 20 ELSE 10 END AS load_max,
      CASE WHEN faction_name = 'ooulium' THEN 5 ELSE 0 END AS warp_efficiency,
      CASE WHEN faction_name = 'carbonae' THEN 5 ELSE 0 END AS fly_efficiency,
      CASE WHEN faction_name = 'sznerckk' THEN 5 ELSE 0 END AS mining_efficiency,
      false,
      false,
      NULL
    FROM loc;
END;
$body$ LANGUAGE plpgsql SECURITY DEFINER;

-- Player creation user

REVOKE ALL ON SCHEMA public FROM public;
ALTER DEFAULT PRIVILEGES REVOKE ALL ON TABLES FROM PUBLIC;
ALTER DEFAULT PRIVILEGES REVOKE ALL ON FUNCTIONS FROM PUBLIC;

CREATE USER player_creator WITH PASSWORD 'player_creator';
GRANT USAGE ON SCHEMA public TO player_creator;
GRANT EXECUTE ON FUNCTION faction() TO player_creator;
GRANT SELECT ON faction TO player_creator;
GRANT EXECUTE ON FUNCTION create_player TO player_creator;
