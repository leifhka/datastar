BEGIN;
\i language.sql
\i schema.sql
\i player_schema.sql
\i change.sql
\i portraits.sql
\i player_user_creation.sql
\i drop_users.sql
\i generate.sql
--\i player_creation.sql
COMMIT;
